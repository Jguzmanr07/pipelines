import os
import requests

private_token = os.getenv('PRIVATE_TOKEN')
gitlab_user_email = os.getenv('GITLAB_USER_EMAIL')
gitlab_user_name = os.getenv('GITLAB_USER_NAME')
version = os.getenv('CI_COMMIT_TAG')
project_id = os.getenv('CI_PROJECT_ID')

headers = {
    "Private-Token": private_token
}

data = {
    "branch": "master", 
    "author_email": gitlab_user_email, 
    'author_name': gitlab_user_name, 
    'commit_message': 'Incrementing version file',
    "content": version
}

r = requests.put('https://gitlab.com/api/v4/projects/%s/repository/files/VERSION' % project_id, data=data, headers=headers)

print r.text